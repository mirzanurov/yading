def binarySearch(alist, item):
        first = 0
	last = len(alist)-1
	found = False
	res=0
	while first<=last and not found:
		midpoint = (first + last)//2
		dif=alist[midpoint] - item
		#if dif  < 0.001:
		if alist[midpoint] == item:
			found = True
			res=midpoint
		else:
			if item < alist[midpoint]:
				last = midpoint-1
			else:
				first = midpoint+1


	if res == 0:
		return last
	else:
		return res

testlist = [0, 1, 2, 8, 13, 17, 19, 32, 42]

kk=binarySearch(testlist, 7)
kl=binarySearch(testlist, 13)
print kk
print kl
