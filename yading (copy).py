# -*- coding: utf-8 -*-
from __future__ import division
import re
import numpy as np
import time
import matplotlib.pyplot as plt
from matplotlib.mlab import PCA as pcalib
import math
import random
from scipy import spatial

ls=[0.5373,0.5311,0.5285,0.5294,0.5336,0.5409,0.5511,0.564,0.5796,0.5976,0.6179,0.6402,0.6644,0.6904,0.718,0.747,0.7773,0.8086,0.8408,0.8737,0.9072,0.9411,0.9752,1.0094,1.0434,1.0772,1.1105,1.1423,1.173,1.2023,1.2302,1.2564,1.2807,1.3031,1.3235,1.3416,1.3573,1.3707,1.3817,1.3901,1.396,1.3993,1.4,1.3982,1.3939,1.3871,1.3779,1.3664,1.3527,1.337,1.3194,1.3001,1.2792,1.2569,1.2334,1.2089,1.1836,1.1577,1.1313,1.1048,1.0781,1.0517,1.0256,1,0.9751,0.9511,0.928,0.9062,0.8857,0.8665,0.8489,0.833,0.8187,0.8061,0.7953,0.7863,0.7791,0.7736,0.7698,0.7677,0.7672,0.7683,0.7708,0.7746,0.7796,0.7857,0.7928,0.8008,0.8095,0.8189,0.8287,0.8389,0.8494,0.8599,0.8705,0.881,0.8913,0.9014,0.9111,0.9204,0.9292,0.9376,0.9455,0.9528,0.9596,0.9658,0.9714,0.9764,0.9809,0.9849,0.9884,0.9915,0.9942,0.9965,0.9986,1.0005,1.0023,1.004,1.0057,1.0074,1.0093,1.0114,1.0137,1.0162,1.0191,1.0223,1.0259,1.0299,1.0343,1.0393,1.0446,1.0505,1.0568,1.0636,1.0709,1.0786,1.0867,1.0952,1.1041,1.1134,1.123,1.133,1.1433,1.1538,1.1646,1.1756,1.1869,1.1982,1.2098,1.2214,1.2331,1.2449,1.2567,1.2685,1.2802,1.2919,1.3035,1.3149,1.3262,1.3372,1.3481,1.3586,1.3689,1.3788,1.3884,1.3976,1.4064,1.4147,1.4225,1.4299,1.4367,1.443,1.4487,1.4537,1.4582,1.462,1.4652,1.4677,1.4695,1.4707,1.4712,1.4711,1.4702,1.4687,1.4665,1.4636,1.46,1.4558,1.4508,1.4453,1.4392,1.4325,1.4252,1.4174,1.4091,1.4003,1.3909,1.3812,1.3709,1.3603,1.3493,1.3379,1.3262,1.3142,1.3019,1.2895,1.2768,1.2641,1.2512,1.2384,1.2257,1.2131,1.2008,1.1887,1.1771,1.166,1.1554,1.1456,1.1365,1.1282,1.1209,1.1147,1.1095,1.1055,1.1028,1.1014,1.1014,1.1027,1.1056,1.1099,1.1156,1.1229,1.1315,1.1414,1.1527,1.1651,1.1786,1.193,1.2084,1.2245,1.2413,1.2585,1.276,1.2936,1.311,1.3281,1.3446,1.3605,1.3753,1.3891,1.4016,1.4126,1.422,1.4296,1.4354,1.4391,1.4408,1.4405,1.4379,1.4333,1.4265,1.4176,1.4067,1.394,1.3794,1.3631,1.3453,1.3262,1.3059,1.2846,1.2625,1.2399,1.2169,1.1938,1.1709,1.1482,1.1261,1.1048,1.0844,1.0652,1.0473,1.0308,1.0159,1.0028,0.9914,0.9818,0.9743,0.9687,0.965,0.9634,0.9637,0.9659,0.9698,0.9754,0.9826,0.9911,1.0008,1.0117,1.0234,1.036,1.0491,1.0627,1.0765,1.0904,1.1043,1.1179,1.1311,1.1439,1.1559,1.1673,1.1778,1.1873,1.1959,1.2033,1.2097,1.2149,1.2189,1.2218,1.2234,1.2239,1.2233,1.2215,1.2187,1.2148,1.21,1.2042,1.1976,1.1902,1.182,1.1732,1.1638,1.1538,1.1433,1.1324,1.121,1.1093,1.0973,1.085,1.0724,1.0596,1.0466,1.0333,1.0198,1.0062,0.9923,0.9782,0.9639,0.9493,0.9346,0.9197,0.9046,0.8893,0.8738,0.8581,0.8422,0.8262,0.81,0.7937,0.7773,0.7608,0.7443,0.7278,0.7114,0.695,0.6788,0.6627,0.6469,0.6314,0.6161,0.6012,0.5868,0.5727,0.5592,0.5462,0.5338,0.522,0.5109,0.5004,0.4905,0.4813,0.4728,0.465,0.4579,0.4514,0.4455,0.4402,0.4355,0.4312,0.4275,0.4242,0.4213,0.4187,0.4164,0.4144,0.4125,0.4107,0.4089,0.4071,0.4053,0.4034,0.4013,0.399,0.3965,0.3937,0.3906,0.3872,0.3835,0.3794,0.3749,0.3701,0.3648,0.3592,0.3531,0.3466,0.3397,0.3325,0.3249,0.3169,0.3086,0.2999,0.2908,0.2814,0.2717,0.2616,0.2511,0.2402,0.2288,0.2171,0.2048,0.1921,0.1789,0.1651,0.1508,0.1358,0.1203,0.104,0.087203,0.069603,0.051403,0.032503,0.013003,-0.0071969,-0.028097,-0.049597,-0.071697,-0.094397,-0.1175,-0.1411,-0.1651,-0.1893,-0.2139,-0.2385,-0.2633,-0.288,-0.3126,-0.337,-0.3611,-0.3848,-0.4081,-0.4308,-0.4528,-0.4742,-0.4947,-0.5144,-0.5332,-0.551,-0.5677,-0.5834,-0.598,-0.6117,-0.6243,-0.6359,-0.6466,-0.6564,-0.6654,-0.6735,-0.681,-0.6878,-0.6941,-0.6999,-0.7052,-0.7103,-0.7151,-0.7197,-0.7242,-0.7287,-0.7332,-0.7378,-0.7425,-0.7474,-0.7525,-0.7579,-0.7635,-0.7695,-0.7757,-0.7823,-0.7892,-0.7963,-0.8037,-0.8114,-0.8193,-0.8273,-0.8356,-0.844,-0.8525,-0.861,-0.8696,-0.8782,-0.8867,-0.8951,-0.9033,-0.9114,-0.9193,-0.927,-0.9344,-0.9415,-0.9483,-0.9549,-0.9611,-0.9669,-0.9725,-0.9777,-0.9826,-0.9871,-0.9912,-0.995,-0.9984,-1.0014,-1.0041,-1.0063,-1.0081,-1.0096,-1.0106,-1.0112,-1.0114,-1.0112,-1.0106,-1.0096,-1.0082,-1.0064,-1.0042,-1.0016,-0.9986,-0.9953,-0.9917,-0.9878,-0.9836,-0.9792,-0.9745,-0.9696,-0.9646,-0.9595,-0.9544,-0.9493,-0.9442,-0.9392,-0.9344,-0.9297,-0.9254,-0.9213,-0.9176,-0.9142,-0.9113,-0.9089,-0.907,-0.9056,-0.9047,-0.9045,-0.9048,-0.9056,-0.9071,-0.9091,-0.9117,-0.9149,-0.9186,-0.9229,-0.9276,-0.9328,-0.9385,-0.9447,-0.9512,-0.9581,-0.9653,-0.9729,-0.9807,-0.9887,-0.997,-1.0054,-1.0141,-1.0228,-1.0318,-1.0408,-1.0499,-1.0592,-1.0686,-1.0781,-1.0877,-1.0975,-1.1074,-1.1174,-1.1276,-1.138,-1.1487,-1.1595,-1.1706,-1.1819,-1.1935,-1.2054,-1.2175,-1.2299,-1.2425,-1.2553,-1.2683,-1.2815,-1.2947,-1.3081,-1.3216,-1.335,-1.3484,-1.3617,-1.3748,-1.3878,-1.4004,-1.4128,-1.4247,-1.4362,-1.4472,-1.4576,-1.4674,-1.4765,-1.4848,-1.4923,-1.499,-1.5049,-1.5098,-1.5137,-1.5168,-1.5189,-1.5201,-1.5204,-1.5198,-1.5183,-1.5161,-1.5131,-1.5094,-1.505,-1.5001,-1.4945,-1.4885,-1.4821,-1.4754,-1.4684,-1.4612,-1.4539,-1.4464,-1.439,-1.4316,-1.4242,-1.4169,-1.4098,-1.4028,-1.3959,-1.3893,-1.3828,-1.3766,-1.3706,-1.3648,-1.3593,-1.3539,-1.3488,-1.3438,-1.339,-1.3343,-1.3298,-1.3254,-1.3211,-1.3169,-1.3127,-1.3086,-1.3046,-1.3007,-1.2968,-1.293,-1.2893,-1.2858,-1.2824,-1.2792,-1.2763,-1.2736,-1.2712,-1.2693,-1.2677,-1.2666,-1.2661,-1.2661,-1.2667,-1.2679,-1.2698,-1.2724,-1.2757,-1.2799,-1.2848,-1.2906,-1.2971,-1.3045,-1.3126,-1.3215,-1.3311,-1.3415,-1.3524,-1.3639,-1.376,-1.3885,-1.4014,-1.4147,-1.4282,-1.4419,-1.4557,-1.4695,-1.4832,-1.4968,-1.5101,-1.5231,-1.5357,-1.5478,-1.5594,-1.5703,-1.5804,-1.5898,-1.5984,-1.6061,-1.6129,-1.6188,-1.6236,-1.6273,-1.63,-1.6315,-1.6318,-1.6309,-1.6287,-1.6253,-1.6206,-1.6147,-1.6074,-1.5989,-1.5891,-1.578,-1.5658,-1.5524,-1.5378,-1.5222,-1.5056,-1.488,-1.4695,-1.4501,-1.43,-1.4091,-1.3877,-1.3657,-1.3433,-1.3206,-1.2976,-1.2744,-1.2513,-1.2283,-1.2054,-1.1829,-1.1608,-1.1392,-1.1182,-1.098,-1.0787,-1.0602,-1.0428,-1.0265,-1.0114,-0.9975,-0.9849,-0.9737,-0.9638,-0.9554,-0.9483,-0.9426,-0.9382,-0.9351,-0.933,-0.9321,-0.9321,-0.933,-0.9346,-0.9369,-0.9397,-0.943,-0.9465,-0.9502,-0.9541,-0.9578,-0.9615,-0.9649,-0.9679,-0.9705,-0.9726,-0.974,-0.9748,-0.9749,-0.9742,-0.9727,-0.9704,-0.9673,-0.9633,-0.9585,-0.9529,-0.9466,-0.9396,-0.9319,-0.9236,-0.9148,-0.9056,-0.8961,-0.8864,-0.8765,-0.8666,-0.8567,-0.847,-0.8375,-0.8283,-0.8195,-0.8111,-0.8033,-0.7959,-0.7892,-0.783,-0.7773,-0.7723,-0.7677,-0.7638,-0.7602,-0.7572,-0.7546,-0.7524,-0.7505,-0.7489,-0.7476,-0.7464,-0.7454,-0.7444,-0.7435,-0.7425,-0.7414,-0.7401,-0.7386,-0.7369,-0.7349,-0.7325,-0.7297,-0.7266,-0.723,-0.7189,-0.7143,-0.7091,-0.7035,-0.6972,-0.6904,-0.683,-0.675,-0.6663,-0.6571,-0.6473,-0.6368,-0.6257,-0.614,-0.6017,-0.5888,-0.5753,-0.5612,-0.5465,-0.5312,-0.5154,-0.4991,-0.4823,-0.4649,-0.4472,-0.429,-0.4104,-0.3915,-0.3723,-0.3528,-0.3332,-0.3135,-0.2938,-0.274,-0.2544,-0.235,-0.2158,-0.1969,-0.1784,-0.1604,-0.143,-0.1261,-0.1099,-0.094497,-0.079897,-0.066097,-0.053297,-0.041397,-0.030397,-0.020497,-0.011497,-0.0033969,0.0037031,0.0099031,0.015303,0.020003,0.023903,0.027203,0.029903,0.032103,0.033903,0.035403,0.036603,0.037603,0.038403,0.039103,0.039903,0.040703,0.041503,0.042503,0.043703,0.045103,0.046703,0.048603,0.050703,0.053103,0.055803,0.058703,0.061903,0.065303,0.069003,0.072803,0.076803,0.080903,0.085103,0.089303,0.093603,0.097803,0.1019,0.106,0.1099,0.1136,0.1172,0.1205,0.1236,0.1265,0.1291,0.1315,0.1337,0.1357,0.1376,0.1395,0.1414,0.1434,0.1456,0.1479,0.1504,0.1533,0.1565,0.16,0.1639,0.1683,0.1731,0.1783,0.184,0.1902,0.1968,0.2039,0.2115,0.2195,0.2279,0.2368,0.2461,0.2558,0.2659,0.2763,0.287,0.298,0.3092,0.3206,0.3321,0.3438,0.3556,0.3673,0.3791,0.3907,0.4022,0.4136,0.4247,0.4355,0.446,0.456,0.4656,0.4749,0.4837,0.4921,0.5,0.5074,0.5143,0.5206,0.5264,0.5314,0.5359,0.5396,0.5426,0.5449,0.5464,0.5471,0.5469,0.5459,0.5439,0.541,0.5372,0.5323,0.5264,0.5195,0.5114]
ls2=[0.5884,0.5939,0.5991,0.6041,0.6088,0.6134,0.6178,0.6221,0.6261,0.6301,0.6339,0.6376,0.6412,0.6447,0.6481,0.6515,0.6548,0.6581,0.6614,0.6646,0.6679,0.6711,0.6744,0.6778,0.6812,0.6846,0.6882,0.6919,0.6958,0.6998,0.704,0.7084,0.713,0.7178,0.7229,0.7281,0.7336,0.7394,0.7453,0.7515,0.7579,0.7645,0.7714,0.7784,0.7856,0.793,0.8005,0.8082,0.816,0.8239,0.8319,0.84,0.8481,0.8562,0.8644,0.8726,0.8808,0.889,0.8972,0.9054,0.9136,0.9218,0.93,0.9381,0.9463,0.9544,0.9624,0.9705,0.9785,0.9866,0.9946,1.0026,1.0107,1.0187,1.0268,1.0349,1.043,1.0511,1.0592,1.0673,1.0754,1.0834,1.0914,1.0994,1.1072,1.115,1.1226,1.1301,1.1374,1.1445,1.1514,1.1581,1.1645,1.1706,1.1764,1.1818,1.1869,1.1916,1.1958,1.1997,1.2031,1.2061,1.2086,1.2107,1.2124,1.2137,1.2145,1.2151,1.2153,1.2151,1.2148,1.2141,1.2133,1.2123,1.2112,1.2099,1.2086,1.2073,1.206,1.2047,1.2035,1.2023,1.2013,1.2004,1.1997,1.1991,1.1987,1.1986,1.1986,1.1988,1.1992,1.1998,1.2006,1.2015,1.2027,1.204,1.2054,1.2069,1.2086,1.2103,1.2121,1.2139,1.2157,1.2175,1.2193,1.2211,1.2228,1.2243,1.2258,1.2272,1.2285,1.2296,1.2306,1.2314,1.2322,1.2328,1.2333,1.2337,1.2339,1.2341,1.2342,1.2343,1.2342,1.2342,1.2341,1.2339,1.2338,1.2337,1.2336,1.2335,1.2335,1.2335,1.2336,1.2337,1.2339,1.2342,1.2345,1.2349,1.2353,1.2358,1.2363,1.2369,1.2375,1.2381,1.2387,1.2393,1.2399,1.2404,1.2409,1.2413,1.2417,1.242,1.2422,1.2423,1.2423,1.2421,1.2419,1.2416,1.2411,1.2406,1.2399,1.2392,1.2384,1.2375,1.2366,1.2357,1.2348,1.2339,1.2331,1.2324,1.2318,1.2314,1.2312,1.2312,1.2315,1.232,1.2329,1.234,1.2356,1.2375,1.2399,1.2426,1.2458,1.2495,1.2536,1.2581,1.263,1.2684,1.2742,1.2804,1.2869,1.2938,1.3009,1.3083,1.316,1.3238,1.3317,1.3397,1.3477,1.3557,1.3636,1.3713,1.3789,1.3862,1.3932,1.3998,1.4061,1.4119,1.4172,1.422,1.4262,1.4298,1.4329,1.4353,1.4371,1.4382,1.4387,1.4385,1.4377,1.4363,1.4343,1.4317,1.4286,1.4249,1.4208,1.4163,1.4113,1.4061,1.4005,1.3947,1.3887,1.3825,1.3763,1.37,1.3638,1.3576,1.3515,1.3456,1.3398,1.3343,1.329,1.324,1.3193,1.3149,1.3108,1.307,1.3036,1.3004,1.2976,1.2951,1.2928,1.2908,1.289,1.2874,1.286,1.2846,1.2833,1.282,1.2807,1.2793,1.2778,1.2762,1.2743,1.2722,1.2698,1.2671,1.264,1.2604,1.2565,1.2521,1.2472,1.2418,1.236,1.2296,1.2228,1.2154,1.2076,1.1993,1.1905,1.1813,1.1717,1.1618,1.1516,1.141,1.1303,1.1193,1.1082,1.0971,1.0858,1.0746,1.0633,1.0522,1.0411,1.0302,1.0195,1.0089,0.9986,0.9886,0.9788,0.9692,0.96,0.951,0.9423,0.9338,0.9257,0.9177,0.9101,0.9026,0.8953,0.8883,0.8814,0.8746,0.868,0.8614,0.8549,0.8484,0.842,0.8354,0.8288,0.8221,0.8153,0.8083,0.8012,0.7939,0.7863,0.7785,0.7704,0.762,0.7533,0.7443,0.7349,0.7253,0.7152,0.7049,0.6941,0.683,0.6715,0.6596,0.6474,0.6348,0.6218,0.6084,0.5947,0.5806,0.5661,0.5514,0.5362,0.5208,0.5051,0.4891,0.4729,0.4564,0.4396,0.4225,0.4053,0.388,0.3706,0.3533,0.336,0.3189,0.302,0.2854,0.2689,0.2528,0.237,0.2215,0.2064,0.1917,0.1773,0.1633,0.1497,0.1365,0.1236,0.1111,0.099,0.0872,0.0756,0.0643,0.0533,0.0424,0.0318,0.0212,0.0108,0.0003001,-0.0101,-0.0205,-0.0311,-0.0417,-0.0526,-0.0636,-0.0749,-0.0864,-0.098299,-0.1105,-0.1231,-0.136,-0.1494,-0.1631,-0.1773,-0.1919,-0.2068,-0.2222,-0.238,-0.2541,-0.2706,-0.287,-0.3034,-0.3197,-0.3359,-0.3521,-0.3681,-0.3839,-0.3997,-0.4152,-0.4307,-0.4459,-0.4611,-0.476,-0.4908,-0.5055,-0.52,-0.5344,-0.5486,-0.5628,-0.5768,-0.5909,-0.6048,-0.6188,-0.6328,-0.6468,-0.6609,-0.6751,-0.6893,-0.7038,-0.7184,-0.7332,-0.7482,-0.7634,-0.7789,-0.7946,-0.8106,-0.8269,-0.8434,-0.8601,-0.8771,-0.8943,-0.9117,-0.9292,-0.9469,-0.9647,-0.9824,-1.0002,-1.0179,-1.0354,-1.0527,-1.0697,-1.0864,-1.1025,-1.1183,-1.1337,-1.1487,-1.1632,-1.1773,-1.1908,-1.2039,-1.2164,-1.2283,-1.2398,-1.2506,-1.2609,-1.2706,-1.2798,-1.2884,-1.2965,-1.304,-1.3111,-1.3177,-1.3239,-1.3297,-1.335,-1.34,-1.3447,-1.3491,-1.3532,-1.3571,-1.3607,-1.3642,-1.3675,-1.3706,-1.3737,-1.3766,-1.3795,-1.3822,-1.3849,-1.3876,-1.3902,-1.3928,-1.3953,-1.3978,-1.4003,-1.4027,-1.4051,-1.4074,-1.4097,-1.4119,-1.4141,-1.4162,-1.4183,-1.4202,-1.4221,-1.424,-1.4258,-1.4275,-1.4291,-1.4307,-1.4322,-1.4336,-1.435,-1.4363,-1.4375,-1.4388,-1.4399,-1.4411,-1.4423,-1.4434,-1.4446,-1.4457,-1.4469,-1.4481,-1.4494,-1.4507,-1.452,-1.4535,-1.455,-1.4565,-1.4582,-1.4599,-1.4617,-1.4635,-1.4655,-1.4675,-1.4696,-1.4717,-1.4739,-1.4761,-1.4783,-1.4806,-1.4829,-1.4852,-1.4875,-1.4897,-1.492,-1.4941,-1.4962,-1.4983,-1.5002,-1.502,-1.5037,-1.5052,-1.5066,-1.5077,-1.5087,-1.5095,-1.5101,-1.5105,-1.5106,-1.5105,-1.5102,-1.5095,-1.5087,-1.5076,-1.5062,-1.5046,-1.5027,-1.5006,-1.4983,-1.4958,-1.4931,-1.4902,-1.4871,-1.484,-1.4807,-1.4774,-1.474,-1.4706,-1.4672,-1.4638,-1.4605,-1.4573,-1.4542,-1.4513,-1.4486,-1.4461,-1.4438,-1.4418,-1.4401,-1.4387,-1.4376,-1.4369,-1.4365,-1.4365,-1.4368,-1.4375,-1.4386,-1.44,-1.4417,-1.4438,-1.4462,-1.4488,-1.4517,-1.4549,-1.4582,-1.4617,-1.4654,-1.4691,-1.473,-1.4768,-1.4806,-1.4844,-1.4881,-1.4916,-1.4949,-1.498,-1.5008,-1.5033,-1.5055,-1.5073,-1.5086,-1.5095,-1.51,-1.51,-1.5095,-1.5084,-1.5068,-1.5047,-1.502,-1.4988,-1.495,-1.4907,-1.4858,-1.4805,-1.4746,-1.4682,-1.4614,-1.4541,-1.4464,-1.4383,-1.4299,-1.421,-1.4119,-1.4025,-1.3928,-1.3829,-1.3728,-1.3625,-1.3521,-1.3415,-1.3308,-1.32,-1.3091,-1.2982,-1.2872,-1.2762,-1.2651,-1.254,-1.2429,-1.2318,-1.2207,-1.2095,-1.1982,-1.187,-1.1756,-1.1642,-1.1527,-1.1411,-1.1294,-1.1177,-1.1058,-1.0938,-1.0816,-1.0694,-1.057,-1.0445,-1.0319,-1.0193,-1.0065,-0.9937,-0.9808,-0.9679,-0.955,-0.9421,-0.9293,-0.9165,-0.9039,-0.8914,-0.879,-0.8669,-0.855,-0.8434,-0.8321,-0.8211,-0.8104,-0.8002,-0.7903,-0.7809,-0.7719,-0.7634,-0.7554,-0.7478,-0.7407,-0.7341,-0.728,-0.7223,-0.7171,-0.7124,-0.7081,-0.7043,-0.7008,-0.6977,-0.6949,-0.6924,-0.6901,-0.6882,-0.6864,-0.6848,-0.6833,-0.6819,-0.6806,-0.6794,-0.6781,-0.6769,-0.6756,-0.6743,-0.6729,-0.6714,-0.6698,-0.6681,-0.6663,-0.6643,-0.6622,-0.66,-0.6576,-0.6551,-0.6525,-0.6497,-0.6467,-0.6436,-0.6404,-0.637,-0.6335,-0.6299,-0.6262,-0.6224,-0.6184,-0.6143,-0.6101,-0.6059,-0.6015,-0.597,-0.5924,-0.5878,-0.583,-0.5782,-0.5732,-0.5682,-0.5632,-0.558,-0.5528,-0.5475,-0.5422,-0.5368,-0.5314,-0.5259,-0.5204,-0.5149,-0.5093,-0.5038,-0.4983,-0.4929,-0.4874,-0.4821,-0.4768,-0.4715,-0.4664,-0.4614,-0.4565,-0.4517,-0.4471,-0.4426,-0.4383,-0.4341,-0.43,-0.4262,-0.4225,-0.4189,-0.4155,-0.4122,-0.409,-0.406,-0.403,-0.4001,-0.3974,-0.3946,-0.3919,-0.3892,-0.3865,-0.3837,-0.3809,-0.3781,-0.3751,-0.372,-0.3688,-0.3654,-0.3618,-0.358,-0.3539,-0.3496,-0.345,-0.3401,-0.3348,-0.3293,-0.3233,-0.3171,-0.3104,-0.3034,-0.2961,-0.2884,-0.2804,-0.272,-0.2634,-0.2545,-0.2454,-0.236,-0.2264,-0.2167,-0.2069,-0.197,-0.1871,-0.1772,-0.1674,-0.1577,-0.1481,-0.1387,-0.1297,-0.1209,-0.1125,-0.1046,-0.096999,-0.09,-0.0836,-0.0777,-0.0724,-0.0678,-0.0638,-0.0606,-0.058,-0.0561,-0.0549,-0.0544,-0.0546,-0.0554,-0.0567,-0.0586,-0.061,-0.0638,-0.0669,-0.0704,-0.0741,-0.0779,-0.0818,-0.0857,-0.0896,-0.0933,-0.096799,-0.1001,-0.103,-0.1055,-0.1075,-0.109,-0.11,-0.1103,-0.1099,-0.1089,-0.1071,-0.1046,-0.1013,-0.097099,-0.0922,-0.0866,-0.0801,-0.073,-0.0651,-0.0566,-0.0475,-0.0378,-0.0276,-0.0169,-0.0057999,0.0056001,0.0174,0.0293,0.0414,0.0535,0.0657,0.0778,0.0897,0.1015,0.113,0.1243,0.1351,0.1456,0.1557,0.1652,0.1742,0.1827,0.1906,0.1979,0.2045,0.2105,0.2158,0.2204,0.2244,0.2277,0.2304,0.2324,0.2338,0.2346,0.2348,0.2345,0.2336,0.2322,0.2304,0.2281,0.2255,0.2226,0.2194,0.2159,0.2122,0.2084,0.2045,0.2005,0.1964,0.1924,0.1885,0.1846,0.1809,0.1774,0.1741,0.1711,0.1683,0.1658,0.1636,0.1618,0.1603,0.1593,0.1586,0.1583,0.1585,0.1591,0.1602,0.1616,0.1635,0.1657,0.1684,0.1715,0.1751,0.1791,0.1836,0.1885,0.194,0.1999,0.2063,0.2133,0.2208,0.2288,0.2374,0.2465,0.2562,0.2665,0.2774,0.2888,0.3009,0.3136,0.3269]
ln=len(ls)
ln2=len(ls2)


fileorigin='StarLightCurves_test_without_labels'

sample=[]
to_assign=[]
local_minimums=set()
s= 2000 # sampling size
d=0 #framelength
percintile=0.8
density_radiuses = []
k_distances = []
def print_test():
    print "Test message"

filesample="sample"
fileassign="toassign"
filekdis="k-dis"

inf_points = [1053, 462, 1623]
density_radiuses_real = [171.4411696166992, 431.8691502197268, 701.6829678344732]

SNG={}  # SNG
geo_clusters = {} # To store clusters 


#variables
coordinates = []
geo_assigned = []

min_neighbors = 4 # our min num of neighbors
geo_visited = []
noise = []
#current density radius
cur_density = 0

neighbour_points_with_distance=[]
tree=[] # var to store KD-tree Data Structure

iter_num_dbscan = 0

def initialisation(filename):
    global sample
    fs = open(filename, "r")
    k=0 # counter of number of lines
    for line in fs:
        line=line.strip(' \t\n')
        splitt=line.split(',')
        #print splitt
        temp_list = []
        bol=False
        for i in splitt:
            try:
                val=float(i)
                bol=True
            except:
                bol=False
                
            if bol:
               temp_list.append(val)
        #print temp_list
            
        if k<s:
            sample.append(temp_list)
            k=k+1
        else:
            break
    fs.close()
    print len(sample)
    #print sample
    '''
    test=np.asarray(sample[0:1])
    #print len(test)
    
    Fg=np.fft.fft(ls)
    conj_Fg=np.fft.fft(Fg.conj())
    res5=np.arange(len(test))
    '''

def auto_estimation_d():
    global local_minimums
    global d
    jj=0 # counter
    for list_ts in sample:

        ls=np.asarray(list_ts)
        Fg=np.fft.fft(ls)  # FFT 
        conj_Fg=np.fft.fft(Fg.conj()) # conj complex transponse
        magnitude_S= Fg*conj_Fg  # magnitude of TS (TS-time series)
        inverse=np.fft.ifft(magnitude_S)
        #if jj % 1000 == 0:
        '''
        if (jj % 200==0) or (jj==s-1):
            print "ok"
            len_list=len(list_ts)
            res8=np.arange(len_list)
            #plot
            plt.plot(res8, inverse.real, 'b-', res8, inverse.imag, 'r--')
            plt.legend(('real', 'imaginary'))
            #plt.show()
            plt.savefig(str(jj)+'.png')
            plt.close()
        '''

        loc_min=np.r_[True, inverse[1:] < inverse[:-1]] & np.r_[inverse[:-1] < inverse[1:], True]


        loc_min=loc_min[1:] #first value always will be true - slice it
        loc_y=0 # local minimum
        for i,val in enumerate(loc_min):
            if val == True: 
                loc_y=inverse[i+1]
                break

        local_minimums.add(loc_y)
        
        jj=jj+1 # increase


    # after getting all locals - sort
    local_minimums=sorted(local_minimums)

    temp=percintile*len(local_minimums)
    nearest=temp%1  # d:  we can use different methods to find percentile
                    #     we will use nearest value as percentile
    
    if nearest >= 0.5:
        index_d = temp//1 # + 1 - 1 because we starts from zero   
    else:
        index_d = temp//1 -1 #   - 1 because we starts from zero 
    index_d=int(index_d)

    d = local_minimums[index_d]
    
    if np.iscomplex(d):
        d=d.real

    nearest=d%1  # TO convert to int
    
    if nearest >= 0.5:
        d = d//1 +1 #   
    else:
        d = d//1 
    d=int(d)

    print "auto frame ", d
    
    # auto frame d=  625.450585205 => 625
    # jk
    
def PAA(reduce_list):
    D= len(reduce_list) # dimension
    # d - length of frame
    reduced=[] # returned value
    for j in range(1,d+1):
        low_bound= (D/d)*(j-1) + 1
        upper_bound = (D/d)*j
        low_bound= int(low_bound)
        upper_bound = int(upper_bound)
	
	#print "low_bound= ", low_bound
	#print "upper_bound= ", upper_bound
        sum_res = 0 
        for k in range(low_bound,upper_bound+1):
            if low_bound == upper_bound:
                #print "double ", k, " sum"
                sum_res=reduce_list[k-1]*2
            else:
                #print k, " sum"
                sum_res=sum_res+reduce_list[k-1]
		
	#print "sum"
        sum_res=sum_res*(d/D)
        reduced.append(sum_res)

    return reduced

         
        

def random_sampling():
    pass
    #global sample
    

def dimensionality_reduction(fileopen, temp):
    global sample
    global to_assign
    sample=[]
    to_assign=[]
    forig = open(fileopen, "r")
    fsample = open(filesample, "w+")
    fassign = open(fileassign, "w+")
    
    k=0 # counter of number of lines
    for line in forig:
        line=line.strip(' \t\n')
        splitt=line.split(',')
        temp_list = []
        bol=False
        for i in splitt:
            try:
                val=float(i)
                bol=True
            except:
                bol=False
                
            if bol:
               temp_list.append(val)
        #print temp_list

        list_reduced=PAA(temp_list)
        
        if k<s:
            
            sample.append(list_reduced)
            k=k+1
            str_to_file=str(list_reduced)
            str_to_file=str_to_file[1:len(str_to_file)-1]
            print >> fsample, str_to_file
        else:
            if k==s:
                print k
            to_assign.append(list_reduced)
            k=k+1
            str_to_file=str(list_reduced)
            str_to_file=str_to_file[1:len(str_to_file)-1]
            print >> fassign, str_to_file
    print k       
    forig.close()
    fsample.close()
    fassign.close()

def read_sample():
    pass
    global sample
    fsample = open(filesample, "r")
    k=0 # counter of number of lines
    for line in fsample:
        line=line.strip(' \t\n')
        splitt=line.split(',')
        #print splitt
        temp_list = []
        bol=False
        for i in splitt:
            try:
                val=float(i)
                bol=True
            except:
                bol=False   
            if bol:
               temp_list.append(val)
        '''
        if k<10:
            sample.append(temp_list)
            k=k+1
        else:
            break
        '''
        sample.append(temp_list)
        
    fsample.close()
    #print sample[0:2]
    print "Length of sample ", len(sample)
    



def PCA():
    data = np.array(sample)
    #print sample
    #print data
    results = pcalib(data.T)
    plt.plot(results.Y, 'o', markersize=7, color='blue')
    plt.show()

def L1_distance(ts1, ts2):
    distance=0
    for jj in range(0,len(ts1)):
        distance += abs(ts1[jj]-ts2[jj])
    return distance

def K_distances():
    
    # we specify k=4
    global k_distances # distance to kNN of object
    #k_distances = []
    ts_distances=[]    # distance between one TS to others
    for i in range(0,len(sample)):
        ts_distances=[]
        for j in range(0,len(sample)):
            distance = L1_distance(sample[i], sample[j])
            #print "dist btween", i, "and", j, "ts - ", distance
            ts_distances.append(distance)
            
        ts_distances.sort()
        # get the 4-th NN distance
        # because we find distance between 2 equal ts
        # we take 5-th NN (because the smallest always was 0
        k = 4 + 1 # 4 - # of k, 1 - to deal with zero
        k_nn_dist = ts_distances [k-1] # indexing from zero

        #print i, "th", "kNN is", k_nn_dist
        
        k_distances.append(distance)
    #  kNN dist-es for all data
    #  sort them in desc order
    k_distances.sort(reverse=True)
    #print k_distances
    #len_list=len(list_ts)
    
    '''point_indx=np.arange(len(k_distances))
    #plot
    plt.plot(point_indx, k_distances)
    plt.legend(('k-dis', 'Point Index'))
    #plt.show()
    plt.savefig('k-dis-curve-sample.png')
    #plt.close()
    '''
    
    fkdis=open(filekdis, "w+")
    str_to_file=str(k_distances)
    str_to_file=str_to_file[1:len(str_to_file)-1]
    print >> fkdis, str_to_file
        
            

def read_k_dis():
    global k_distances
    fkdis = open(filekdis, "r")
    k=0 # counter of number of lines
    for line in fkdis:
        line=line.strip(' \t\n')
        splitt=line.split(',')
        #print splitt
        temp_list = []
        bol=False
        for i in splitt:
            try:
                val=float(i)
                bol=True
            except:
                bol=False   
            if bol:
               temp_list.append(val)
        '''
        if k<10:
            sample.append(temp_list)
            k=k+1
        else:
            break
        '''

    k_distances=temp_list
    #print k_distances
    #print len(k_distances)

def slope(higher_point_index,lower_point_index):
    slope_value = 0
    slope_value = abs(k_distances[higher_point_index]-k_distances[lower_point_index])
    
    return slope_value
    
def inflectionpoint(s,e):
    r=-1
    diff=-1
    for i in range (s,e):
        left = slope(s,i)
        right=slope(i,e)
        
        

def densityradiuses():
    global density_radiuses
    length = len(k_distances)
    inflectionpoint(0, length)
    # return density_radiuses
    
    #return radiuses()


def neighbors(p,ind):

    global neighbour_points_with_distance
    neighbour_points_with_distance=[]
    
    data = tree.query(p, distance_upper_bound = cur_density, k = len(sample), p=1) #Manh dist

    near_neigh=list(data[1][0:len([dist for dist in data[0] if dist < float('inf')])])

    if len(near_neigh) > min_neighbors:
        item_in=0
        temp_points=[]
        temp_points = list(sample)
        temp_points.remove(p)
        for jjj in range(len(temp_points)):
            distance=L1_distance(p, temp_points[jjj])
            neighbour_points_with_distance.append([jjj,distance])

        neighbour_points_with_distance=sorted(neighbour_points_with_distance,key=lambda l:l[1])
    else:
        pass
    

    return near_neigh


def expand_cluster(point, cluster, n, index):
    global geo_clusters
    global geo_assigned
    global geo_visited
    global noise 
    global SNG
    
    if cluster not in geo_clusters:
        geo_clusters[cluster] = []


    geo_clusters[cluster].append(index)# we stored it in class
    geo_assigned[index] = True
    candidates = n
    k=0

    # try all candidates
    while len(candidates)!=0:
        buf = []
        k=0
        print len(candidates), "len can"
        for p_index in candidates:
            if p_index not in geo_visited:
                #print p_index
                geo_visited.append(p_index)
                k=k+1
                #print k, "k"
                n1 = neighbors(sample[p_index],p_index)

                if len(n1) < min_neighbors:
                    noise.append(p_index)
				# if candidate is good for us and not visited
                else:
                    buf += n1

					# if he is not belong to another cluster
                    if not geo_assigned[p_index]:
                        SNG[p_index]=[neighbour_points_with_distance]
                        geo_clusters[cluster].append(p_index)
                        geo_assigned[p_index] = True
		# calculate candidates
        candidates = buf
        #print candidates, "aftercan"

        
    
def dbscan(coordinates, dens_r):
    global iter_num_dbscan

    iter_num_dbscan+=1
    
    
    global geo_visited
    geo_visited=[]

    global cur_density
    cur_density = dens_r

    global noise
    noise=[]

    global SNG
    
    global geo_clusters
    
    print "Number of DBSCAN - ", iter_num_dbscan
    
    
    k4=0
    lengthcor=len(coordinates)
    print lengthcor
    for i in coordinates:
        if i not in geo_visited:
            k4=k4+1 # num of being in this main proc
            print i, "visitnow"
            geo_visited.append(i)

            n = neighbors(sample[i],i)
                    
            if len(n) < min_neighbors:
                noise.append(i)
                print "noise"
            else:

                SNG[i]=[neighbour_points_with_distance]
                clusers = geo_clusters.keys()
                cluster = max(clusers) if clusers else 0
                geo_clusters[cluster + 1] = []
                expand_cluster(sample[i], cluster, n, i)


    # printing results
    print "How much clusters ", len(geo_clusters)
    #print "How much time it was in main proc", k4

    # len of the data
    bb=0
    for i in geo_clusters.keys():
        bb=bb+len(geo_clusters[i])
        print "Cluster label ", i
        if len(geo_clusters[i]) < min_neighbors:
            for el in geo_clusters[i]:
                noise.append(el)
            geo_clusters = {key: value for key, value in geo_clusters.items() if key != i}


    clusers = geo_clusters.keys()
    cluster = max(clusers) if clusers else 0
    geo_clusters[cluster + 1] = []

        #if len(geo_clusters[i]) == 0:
            #geo_clusters = {key: value for key, value in geo_clusters.items() if key != i}
    print "Length of data ", bb

    print "How much clusters ", len(geo_clusters)    

    print "Clusters"
    print geo_clusters

    print 'Starts noise'
    print len(noise), "number of noise"
    print noise
    

    print "____________________________"
    #print SNG

    #return geo_clusters
	




def multi_dbscan():
    global coordinates
    coordinates = [x for x in xrange(0, len(sample), 1)]

    global tree
    tree = spatial.cKDTree(sample)

    global geo_assigned
    for i in range(len(coordinates)):
        geo_assigned.append(False)

    global noise
        
    global geo_clusters

    for dens_r in density_radiuses_real:
        print dens_r
        dbscan(coordinates, dens_r)
        temp=[]
        for i in geo_clusters.keys():
            for elem in geo_clusters[i]:
                temp.append(elem)
        
        
        for val in temp:
            if val in coordinates:
                coordinates.remove(val)

    for i in geo_clusters.keys():
        if len(geo_clusters[i]) == 0:
            geo_clusters = {key: value for key, value in geo_clusters.items() if key != i}



    noise = []
    noise = list(coordinates)

    #print "SNG"
    #print SNG

#def




if __name__ == '__main__':

    start = time.time()

    print_test()
    #initialisation(fileorigin)
    #auto_estimation_d()
    #PAA()

    #dimensionality_reduction(fileorigin, 'temp')

    read_sample() # read first 10 - to test - can be changed
    #PCA()
    #K_distances()

    #read_k_dis()

    multi_dbscan()

    '''
    ASSIGNMENT(SNG, uObj)
for each obj ∈ uObj
set the label of obj as “noisy”
for each o ∈ {keys of SNG}
if o has been inspected
continue;
dis  L1 distance between o and obj
if dis less than density radius of o
mark obj with same label of o
break
mark o as inspected
jump  dis - density radius of o
i  BINARYSEARCH(SNG[o], jump)
for each neighbor ∈ SNG[o] with index greater than i
if density radius of neighbor is less than jump
mark neighbor as inspected
else break /*this is a sorted list*/ASSIGNMENT(SNG, uObj)
for each obj ∈ uObj
set the label of obj as “noisy”
for each o ∈ {keys of SNG}
if o has been inspected
continue;
dis  L1 distance between o and obj
if dis less than density radius of o
mark obj with same label of o
break
mark o as inspected
jump  dis - density radius of o
i  BINARYSEARCH(SNG[o], jump)
for each neighbor ∈ SNG[o] with index greater than i
if density radius of neighbor is less than dens radius of o
mark neighbor as inspected
else break /*this is a sorted list*/

    '''
    

    # 3. For each radius from bigger to smaller: TODO: DBSCAN + SNG
    # It will took time
    # DONE: 1. Find k-dis for them
    # DONE: 2. Sort in desc order
    
    # DONE: read k-dis from file
    # DONE: dens - radiuses
    # DONE: inf points

    
    # TODO: DBSCAN
    # TODO: move code from inflection.py to yading.py
    end = time.time()
    
    print "Performance time is: ", end - start 
    # Performence time is:  258.590808868
    # Performence time is:  258.464388847

    
    
    '''
    res2=np.arange(ln)
    ls=np.asarray(ls)
    Fg=np.fft.fft(ls)
    conj_Fg=np.fft.fft(Fg.conj())



    #magnitude_S= Fg*conj_Fg

    print magnitude_S

    inverse=np.fft.ifft(magnitude_S)

    #print inverse
    
    plt.plot(res2, inverse.real, 'b-', res2, inverse.imag, 'r--')
    plt.legend(('real', 'imaginary'))
    #plt.show()


    loc_min=np.r_[True, inverse[1:] < inverse[:-1]] & np.r_[inverse[:-1] < inverse[1:], True]


    loc_min=loc_min[1:]
    print loc_min[0:5]
    print inverse[0:5]
    loc_y=0
    for i,val in enumerate(loc_min):
        if val == True:
            print 
            loc_y=inverse[i+1]
            break
    print loc_y

    local_minimums.add(loc_y)
    
    local_minimums.add(544)
    local_minimums.add(200)
    local_minimums.add(700)

    print "local_minimums", local_minimums

    local_minimums=sorted(local_minimums)

    print "local_minimums_sorted", local_minimums

    temp=percintile*len(local_minimums)
    nearest=temp%1  # d:  we can use different methods to find percentile
                    #     we will use nearest value as percentile
    
    if nearest >= 0.5:
        index_d = temp//1 # + 1 - 1 because we starts from zero   
    else:
        index_d = temp//1 -1 #   - 1 because we starts from zero 
    index_d=int(index_d)

    d = local_minimums[index_d]


    print "auto frame ", d
    
    if np.iscomplex(d):
        print "complex"
        d=d.real
    else:
        print "not"
    
    print "auto frame ", d   
    
    '''
    

        
    '''
    1. Read file
    2. Read first s lines
    3. Apply auto-estimation
    4. Find d
    
    5. Open file
    6. Apply PAA
    7. save new file
    8. Apply data sampling to new file
    9. Get 2 files - sample and rest.
    
    '''
    
    
        
    # DONE: collect all loc_y
    # DONE: sort them
    # DONE: get 80 percentile as d = length of each entity - based on sample
    # DONE: apply PAA to all dataset (N) - sample and other
    # DONE: create sample files and other dataset.
    # TO_TEST: How to pick the sample? We just take first s rows (2000)
    # TODO: Write MUlytiDEBISCAN
    # TODO: Write function to find several density radiuses
    

    
    #plt.plot(res2, inverse.real, 'b-', res2, inverse.imag, 'r--')
    #plt.legend(('real', 'imaginary'))
    #plt.show() 
    
    '''
    print Fg
    print
    print "================"
    print
    print conj_Fg
    print len(Fg)
    print
    print len(conj_Fg)

    #print Fg[0]
    #print Fg

    #print Fg.size
    '''
   
    '''
    print ln

    #res = [x for x in xrange(0, ln, 1)]

    #print res

    res2=np.arange(0,ln, 0.001)

    res3=np.linspace(0,1, ln)

    res4=np.linspace(0,1, ln2)
    #for i in res3:
        #print i


    plt.plot(res2, ls)
    #plt.plot(res4, ls2)
    plt.show()
    '''

    
