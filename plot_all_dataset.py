# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import PCA as pcalib

fileorigin='Two_Patterns_test_without_labels'

fs = open(fileorigin, "r")
k=0 # counter of number of lines
for line in fs:
        line=line.strip(' \t\n')
        splitt=line.split(',')
        #print splitt
        temp_list = []
        bol=False
        for i in splitt:
                try:
                        val=float(i)
                        bol=True
                except:
                        bol=False
                if bol:
                    temp_list.append(val)
        #print temp_list
        len_list=len(temp_list)
        res8=np.arange(len_list)
        plt.plot(res8,temp_list)

fs.close()
plt.show()



